#!/usr/bin/env bash

IMAGENAME="todoapp"

cd $(dirname $(readlink -e $0))
docker build -t $IMAGENAME .